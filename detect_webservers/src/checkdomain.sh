#!/bin/bash
###
# Copyright (c) 2011-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# checkdomain
# this script takes an organization domain and an optional linked domain
# and retrieves organization information and checks if the linked domain
# is likely to belong to the organisation.
#
# This script is not very portable. Uses tools that have implementation
# dependent functionality. sed is different from gnu sed eg. whois output
# is different on Mac vs Linux ...

set -eu -o pipefail
shopt -s failglob

# Variables
# parameters
DOMAIN='' # organization domain
LINK='' # linked domain
SCAN='' # enable domain scan
# domain related
CIDR='' # cidr network address ranges by ARIN
ROUTE='' # network address range by RIPE
IP='' # domain ip through nslookup
NSERVER='' # domain's name server
REGISTRAR='' # domain's registrar information
EMAIL='' # domain's registrar email domain
# link related
AUTHORITY='' # the links authoritative domain
LIP='' # link ip address
LNSERVER='' # array of link authoritative name servers
LREGISTRAR='' # linked domain's registrar
LEMAIL='' # array of linked domain's registrar email domain

usage() {
	echo "checkdomain -- retrieve organization information, check if link belongs to organization"
	echo "usage: checkdomain -d organization_domain [-l link] [-s]"
	echo
	echo "Options:"
	echo -e "\t-d\torganization_domain, the main domain of the organization, with tld. e.g. uni-konstanz.de"
	echo -e "\t-l\tlink, the link/URL to check, e.g. treetank.org"
	echo -e "\t-s\tstart an nmap network scan of identified organization network" 
	echo
}

prerequisits() {
	[[ -x `which nslookup` ]] || { echo "Error: nslookup not found." >&2; exit 0; }
	[[ -x `which whois` ]] || { echo "Error: whois not found." >&2; exit 0; }
	[[ -x `which dig` ]] || { echo "Error: dig not found." >&2; exit 0; }
}

# TODO: nslookup does not always work on domains! e.g. htwg-konstanz.de, uni-mannheim.de
# configured wrong? browser does find it though ... auto add www.?
# whois [-r | -a] equivalent in ubuntu (only queries RIPE)
# whois output also implementation dependent
chkdomain() {
	[[ -z $DOMAIN ]] && { echo "No domain provided. Nothing to do." >&2; exit -1; } || echo "Retrieving Info on Domain: $DOMAIN";
	CIDR=$(whois -h whois.arin.net `nslookup -type=A www.$DOMAIN | egrep 'Address: ' | cut -d ' ' -f 2` | egrep 'CIDR' | sed -e 's/[ ,][ ]*/ /g' | cut -d ' ' -f 2-)
	ROUTE=$(whois -h whois.ripe.net `nslookup -type=A www.$DOMAIN | egrep 'Address: ' | cut -d ' ' -f 2` | egrep 'route' | sed -e 's/[ ][ ]*/ /g' | cut -d ' ' -f 2)
	IP=`nslookup -type=A www.$DOMAIN | egrep 'Address: ' | cut -d ' ' -f 2`
	#NSERVER=$(whois $DOMAIN | egrep '\<[0-9]+\.[0-9]+\.' | cut -d " " -f 2-3 | sed -e ':a;N;$!ba;s/\n/ /g')
	NSERVER=$(whois $DOMAIN | egrep '\<[0-9]+\.[0-9]+\.' | cut -d " " -f 2 | tr '\n' ' ')
	REGISTRAR=`whois $DOMAIN | egrep 'Address|Email|Registrant Organization|Registrant Street' | sed -e 's/.*://g' | sed -e 's/^ //g' | sed -e '/^$/d'`
	#EMAIL=$(grep -Eio '\b@[A-Z0-9.-]+\.[A-Z]{2,4}\b' <<< $REGISTRAR | sed 's/@//g' | tr '\n' ' ') # verified Mac OS X
	EMAIL=$(echo $REGISTRAR | grep -Eio '\b@[A-Z0-9.-]+\.[A-Z]{2,4}\b' | sed 's/@//g' | tr '\n' ' ') # verified Ubuntu
}

printdomain() {
	[[ -z $DOMAIN ]] && { return 0; } || { echo "Domain Information: $DOMAIN"; }
	echo "CIDR: $CIDR"
	echo "route: $ROUTE"
	echo "IP: $IP"
	echo "Name Server: $NSERVER"
	echo "Registrar: $REGISTRAR"
	echo "Email Domains: $EMAIL"
	echo
}

# there is a difference in using a host like www.treetank.org vs domain like treetank.org
# especially name server, authority, and registrar info is different.
# whois only works with domains properly
# dig / nslookup works better with complete hosts
# so authority migth be wrong with domain
# name servers must be retrieved differently with hosts
chklink() {
	[[ -z $LINK ]] && { echo "No linked domain to check. Bye." >&2; exit 0; } || echo "Checking link: $LINK";
	AUTHORITY=$(dig SOA $LINK | grep SOA | grep -v '^;' |  sed -e "s/[	 ][	 ]*/ /g" | cut -d ' ' -f 1)
	LIP=`nslookup -type=A $LINK | egrep 'Address: ' | cut -d ' ' -f 2`
	LNSERVER=(`echo $(dig +short NS $LINK | tr '\n' ' ')`)
	LREGISTRAR=`whois $LINK | egrep 'Address|Email|Registrant Organization|Registrant Street' | sed -e 's/.*://g' | sed -e 's/^ //g'  | sed -e '/^$/d'`
	#LEMAIL=$(grep -Eio '\b@[A-Z0-9.-]+\.[A-Z]{2,4}\b' <<< $LREGISTRAR | sed 's/@//g' | tr '\n' ' ') # verified Mac OS X
	LEMAIL=(`echo $(echo $LREGISTRAR | grep -Eio '\b@[A-Z0-9.-]+\.[A-Z]{2,4}\b' | sed 's/@//g' | tr '\n' ' ')`)
}

printlink() {
	[[ -z $LINK ]] && { return 0; } || { echo "Link Information: $LINK"; }
	echo "Authority: $AUTHORITY"
	echo "IP: $LIP"
	#echo "Name Server: $LNSERVER"
	echo "Name Server: ${LNSERVER[@]}"
	echo "Registrar: $LREGISTRAR"
	#echo "Email Domains: $LEMAIL"
	echo "Email Domains: ${LEMAIL[@]}"
	echo 
}

scan() {
	[[ -x `which nmap` ]] || { echo "Error: nmap not found. No scan possible"; return; } && echo "Scanning network: $ROUTE"
	nmap -n -p80,443 -Pn -oG - $ROUTE | awk '/open/{print $2}' | sed -e 's/^/<value>/' | sed -e 's/$/<value\/>/'
}

comparedomains() {
	[[ -z $DOMAIN ]] && [[ -z $LINK ]] && { return 0; } || { echo "Compare domain $DOMAIN and link $LINK"; }
	local nchecks=0
	local nmatches=0
	
	# check if ip in network
	local ip_in_net=`./checkip -a ${IP} -n ${ROUTE}`
	nchecks=$(( nchecks + 1 ))
	[[ $ip_in_net=='True' ]] && nmatches=$(( nmatches += 1 ))

	# check if link has domain's webservers
	local equal_ns=0
	#for ns in $LNSERVER; do
	for ns in "${LNSERVER[@]}"; do
		ns=`echo $ns | sed 's/\.$//'`;
		if [[ $NSERVER == *"$ns"* ]]; then
			equal_ns=$(( equal_ns + 1 ))
			nmatches=$(( nmatches += 1 ))
		fi
		nchecks=$(( nchecks + 1 ))
	done;
	
	# check if registrars email addresses match
	local equal_mail=0
	for mail in "${LEMAIL[@]}"; do
		if [[ $EMAIL == *"$mail"* ]]; then
			equal_mail=$(( equal_mail + 1 ))
			nmatches=$(( nmatches += 1 ))
		fi
		nchecks=$(( nchecks + 1 ))
	done;

	# output results
	echo "Link IP in Domain Network: $ip_in_net"
	echo "Equal Name Server: $equal_ns / ${#LNSERVER[@]}"
	echo "Equal Mail Addresses: $equal_mail / ${#LEMAIL[@]}"
	echo "Number of matches / Number of checks: $nmatches / $nchecks"
}

[[ -z "$1" ]] && usage;
prerequisits;

while getopts  "d:l:sh?" opt; do
	#echo "$opt" $OPTIND $OPTARG
	case "$opt" in
		h|\?) usage; exit 0;;
		d) DOMAIN=$OPTARG;;
		l) LINK=$OPTARG;;
		s) SCAN='true';;
	esac
done

if [ ! -z $DOMAIN ]; then
	chkdomain;
	printdomain;
fi
if [ ! -z $LINK ]; then
	chklink;
	printlink;
fi
[[ ! -z $DOMAIN ]] && [[ ! -z $SCAN ]] && scan
[[ ! -z $DOMAIN ]] && [[ ! -z $LINK ]] && comparedomains;
