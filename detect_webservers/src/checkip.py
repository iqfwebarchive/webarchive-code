#!/usr/bin/env python
###
# Copyright (c) 2011-2019 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
"""
Converts IPv4 addresses between strings and integers.
Converts CIDR network notation to bitmasks.
Can check if an IP or subnet is inside a network.

References:
https://gist.github.com/cslarsen/1595135
http://stackoverflow.com/questions/819355/how-can-i-check-if-an-ip-is-in-a-network-in-python
http://stackoverflow.com/questions/3222379/how-to-efficiently-convert-long-int-to-dotted-quad-ip-in-bash/3222521#3222521
http://stackoverflow.com/questions/10768160/ip-address-converter
"""
import sys, optparse

def from_string(s):
	'''convert dotted IPv4 address to integer.'''
	return reduce(lambda a,b: a<<8 | b, map(int, s.split(".")))
        # that is ((byte[0] << 8 | byte[1]) << 8 | byte[2]) << 8 | byte[3]

def to_string(ip):
	'''convert int IP to dotted IPv4 address string'''
	return ".".join(map(lambda n: str((ip >> n) & 0xFF), [24,16,8,0]))

def to_net_mask(bits):
	'''return a mask of n bits as a long integer'''
	return ((2L<<bits-1) - 1)<<(32-bits)

def to_host_mask(bits):
        '''return a mask of n bits as int'''
        return 2**(32-bits)-1

def host_id(dec_ip, bits):
        '''extracts the host id of an address as long int'''
        host_mask = to_host_mask(bits)
        return dec_ip & host_mask
        
def is_host(dec_ip, bits):
        '''check if ip is a valid host'''
        host_mask = to_host_mask(bits)
        return host_mask > dec_ip & host_mask > 0

def is_reserved(dec_ip, bits):
        host_mask = to_host_mask(bits)
        host = dec_ip & host_mask

        return (host == host_mask or host == 0)
        
def address_in_network(dec_ip, net_ip, net_mask):
	"Checks if an ip address is in a network."
 	return dec_ip & net_mask == net_ip & net_mask

def check_ip(str_ip, str_net):
        dec_ip, ip_bits = str_ip.split('/')
	dec_ip = from_string(dec_ip)

        net_ip, net_bits = str_net.split('/')
	net_ip = from_string(net_ip)
	net_bits = int(net_bits)

        net_mask = to_net_mask(net_bits)
        host_mask = to_host_mask(net_bits)

        ishost = is_host(dec_ip,net_bits)
        isreserved = is_reserved(dec_ip,net_bits)
        isinnet = address_in_network(dec_ip, net_ip, net_mask)
 
        return [ishost, isreserved, isinnet]
        

def setup_parser(argv):
        p = optparse.OptionParser()
	p.add_option('-a', dest='address',default='', help='IP address which is compared to network. CIDR notation.')
	p.add_option('-n', dest='network', default='', help='network which is checked if it contains ip, CIDR notation.')
	opts,args = p.parse_args(argv[1:])

	if not opts.address and not opts.network:
                p.print_help()
                sys.exit(1)
	if not opts.address.count(".")==3:
		print "Error: invalid IP address"
		sys.exit(-1)
	if not '/' in opts.address:
                print "Error: address not in CIDR notation"
		sys.exit(-2)
	if not '/' in opts.network:
		print "Error: network not in CIDR notation"
		sys.exit(-2)

	return opts, args

def main (argv):
        opts, args = setup_parser(argv)
	return check_ip(opts.address, opts.network), opts, args

if __name__ == '__main__':
	check, opts, args = main(sys.argv)

	print "Address is host: ", check[0]
        print "Address is reserved: ", check[1]
        print "Address is in network: ", check[2]

