#!/bin/bash
###
# Copyright (c) 2011-2018 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# scan_webservers.sh
# Takes a webserver hostname as argument and scans for webservers in the organization network.
###

set -e -o pipefail
shopt -s failglob

usage() {
	echo "usage: `basename $0` hostname";
	echo "";
	echo "Find all webservers to associated network.";
	exit 0;
}

# check parameters
[[ -z "$1" ]] && (usage; exit 1);
ROOT=$1;

# find associated network ('route' in whois)
ROUTE=$(whois -h whois.ripe.net `nslookup -type=A $ROOT | egrep 'Address: ' | cut -d ' ' -f 2` | egrep 'route' | sed -e 's/[ ][ ]*/ /g' | cut -d ' ' -f 2);

# scan for webservers
nmap -n -p80,443 -Pn -oG - $ROUTE | awk '/open/{print $2}' | sed -e 's/^/<value>/' | sed -e 's/$/<value\/>/'

