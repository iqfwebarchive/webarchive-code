# Detect Webservers

Autodetect webservers that are associated with an organization.
The scripts use heuristics and public data (registrars, nameservers, ip address ranges, email adresses) to identify if a domain or IP is controlled by a specific organization.

## Usage

### checkdomain

Get help:

    ./checkdomain.sh -h
    checkdomain -- retrieve organization information, check if link belongs to organization
    usage: checkdomain -d organization_domain [-l link] [-s]

    Options:
    	-d	organization_domain, the main domain of the organization, with tld. e.g. uni-konstanz.de
    	-l	link, the link/URL to check, e.g. treetank.org
    	-s	start an nmap network scan of identified organization network to find webservers


#### Retrieve information about an organization.

    ./checkdomain.sh -d organization_domain

E.g.

    ./checkdomain.sh -d uni-konstanz.de

#### Check, if a link / URL belongs to an organization

    ./checkdomain.sh -d organization_domain -l link/url

E.g.

    ./checkdomain.sh -d uni-konstanz.de -l bsz-bw.de

### checkip

Check, if an IP address belongs to a specific network.
Get help:

    ./checkip.py -h
    Usage: checkip.py [options]

    Options:
      -h, --help  show this help message and exit
      -a ADDRESS  IP address which is compared to network. Dotted notation.
      -n NETWORK  network which is checked if it contains ip, CIDR notation.
    
