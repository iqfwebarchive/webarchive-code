#!/usr/bin/env python
# -*- coding: utf-8 -*-
###
# Copyright (c) 2011-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
'''
This script demonstrates a simple directory watcher.
It takes a path to a directory as argument.
This path is then constatntly watched for any changes in number of files or
file content. If a change occurs, it is printed on stdout.
'''

import os
import sys
from hashlib import sha1

def githash(data):
	s = sha1()
	s.update("blob %u\0" % len(data))
	s.update(data)
	return s.hexdigest()

def watchdir(dir):
	# check directory content
	lastfiles = os.listdir(dir)
	lastnfiles = len(lastfiles)
	lastchecks = []
	
	# compute checksums
	for fil in lastfiles:
		if os.path.isfile(fil):
			handle = open(fil,'rb')
			data = handle.read()
			lastchecks.append(githash(data))

	# watch directory
	iswatching = True   
	while iswatching:
		# compare directory content
		files = os.listdir(dir)
		nfiles = len(files)
		if nfiles != lastnfiles:
			print "number files changed"
		lastfiles = files
		lastnfiles = nfiles
		
		# compare checksums
		checks = []
		for fil in files:
			if os.path.isfile(fil):
				handle = open(fil,'rb')
				data = handle.read()
				checks.append(githash(data))
			
		if lastchecks != checks:
			print "files changed"
		lastchecks = checks

if __name__ == '__main__':
	if len(sys.argv) != 2:
		print "usage:", sys.argv[0], "<watched_path>"
		sys.exit(0)
	watchdir(sys.argv[1])