# Automatische Erkennung von Crawlingraten und Evaluierung von Push Mechanismen

Ein nicht zu unterschätzendes Problem der Web-Archivierung ist die Festlegung der Crawlingrate, also in welcher Frequenz die Webseiten geharvested werden sollen. Eine zu hohe Rate belastet bei wenig Änderungen der Seite unnötig das Netzwerk und die Server sowohl der Organisation als auch des Crawlers selbst und führt gegebenenfalls zu einer Ansammlung von unnötigen und redundanten Daten. Ist allerdings die Rate zu niedrig und die Anzahl der Änderungen an der Zielwebseite im Vergleich hoch, so können wichtige, eigentlich zu archivierende, Informationen verloren gehen.

Bisher ist eine konstante und manuelle Überwachung und Einstellung der Crawls notwendig, um eine möglichst lückenlose und ressourcenschonende Archivierung zu gewährleisten. Nun stellt sich zwangsläufig die Frage, ob und falls wie es möglich ist, eine möglichst optimale Crawlingrate automatisch zu erkennen. Um die Crawlingrate festzulegen, muss also festgestellt werden wann und wie viele Änderungen an den zu archivierenden Webseiten vorgenommen werden. Es geht also um die automatische Erkennung von Änderungen an den Webseiten.

Hierfür gibt es prinzipiell zwei herangehensweisen, die wir als _Push_ bzw. _Pull Mechanismen_ bezeichnen. Ein Möglichkeit besteht darin, dass die Ziel-Organisation ein Notifikationssystem bereitstellt, welches die zu archivierenden Webseiten überwacht und bei Änderungen den Crawler informiert. Herfür gibt es verschiedene mögliche Mechanismen, welche unter _Push Mechanismen_ diskutiert werden.

Eine zweite Heransgehensweise ist, dass die Crawlende Einheit selbst Mechanismen bereitstellt, um Änderungen schnell und günstig zu erkennen, ohne die gesamten Seiten komplett crawlen zu müssen. Zwangsläufig ergibt sich hierbei ein Sampling Problem, schliesslich soll vermieden werden, alle Seiten prüfen zu müssen und dennoch soll eine gute Repräsentation von Änderungen gefunden werden. Mögliche Mechanismen und deren Umsetzung werden im Abschnitt _Pull Mechanismen_ behandelt.

Abbildung Die folgende Abbildung stellt den Unterschied zwischen Pull und Push Mechanismen graphisch dar. Bei _Push_ sendet der Webserver (oder eine andere überwachende Instanz) Notifikationen über Änderungen an den Crawler, während dieser bei _Pull_ selbst auf Änderungen untersucht.

![Pushl](fig/pushl.pdf)

## Push Mechanismen

Die naheliegendste Möglichkeit zur Feststellung von Änderungen an den Webseiten liegt darin, Änderungen beim Hoster bzw. Webserver der Seiten selbst festzustellen. Da leider die Serverlandschaft sehr heterogen ausfallen kann bedarf es generell unterschiedlicher, spezifisch auf die Webserver angepasster Lösungen.

Eine plattformübergreifende Lösung wäre das Einsetzen eines **Website-Watchers**. Hierfür gibt es beispielsweise zumindest einen [kommerziellen Anbieter](http://www.aignes.com/). Der WebSite-Watcher kann nicht nur Webpages überwachen, sondern auch RSS und News Feed und auch Änderungen lokaler Dateien. Allerdings wirkt dieser prinzipiell genauso wie ein normaler Webcrawler, d.h. die Webseiten werden regelmäßig übers Netz gescannt und mit vorherigen Scans verglichen, um Änderungen festzustellen und Reporte zu generieren. Dies macht natürlich jeglichen Vorteil zu nichte, da die Netzwerklast insgesamt dadurch sogar höher ausfällt, durch zusätzliche Scans.

![Pushl](fig/wswss_main.png)

Tatsächlich machen Push Mechanismen nur dann Sinn, wenn sie lokal auf den Webservern, oder zumindest im lokalen Subnetz, eingesetzt werden, um zusätzliche Last auf dem Netzwerk zu vermeiden oder zu minimieren. Daher beschränken sich die Möglichkeiten auf lokal einsetzbare und plattformabhängige Lösungen.

Ein Produkt speziell für Microsoft Webserver ist der **WebServerFarmManager**, mit welchem mehrere Server innerhalb einer Serverfarm oder einem Cluster überwacht werden können. Allerdings sind die Produktanforderungen recht hoch und auf Grund der Microsoft Zugehörigkeit natürlich beschränkt auf Microsoft Server Produkte. Einen ausführlichen Artikel über die Nutzung des WebServerFarmManagers findet man auf [iis.net](http://blogs.iis.net/moshaikh/archive/2009/04/26/web-server-change-notification-or-web-server-compare-or-multi-server-sync-in-a-web-farm.aspx).

Viele Content-Management-, Wiki- und auch Foren-Systeme, bieten die Möglichkeit, bei Änderungen Notifikationen per eMail zu verschicken. Auch diese können eingesammelt und ausgewertet werden. Auch hierbei ist die Lösung jedoch herstellerabhängig und muss individuell an die Systeme angepasst werden. Jedoch ist dies eventuell eine der wenigen Push-Möglichkeiten bei Datanbankbasierten und dynamisch erzeugten Webseiten.

Eher bietet es sich an, die Daten im Dateisystem selbst auf Änderungen zu untersuchen. Dies erfolgt mit Hilfe sogenannter **File Integrity Checks**. Viele Systeme, besonders unixartige, bringen die dafür notwendigen Werkzeuge bereits mit. Aber auch auf anderern Systemen sind equivalente Werkzeuge generell erhältlich. Auch die Umsetzung ist universell. Ein **File Integrity Checker** überprüft in regelmäßigen Abständen Dateien und Verzeichnisse auf den Webservern, merkt sich stehts das Ergebnis der letzen Prüfung und vergleicht dieses mit dem neuen Ergebnis. Bei einer Änderung der Checksumme wird dann eine Nachricht generiert.

Auch hier ergibt sich wieder die Möglichkeit eine zentrale überwachende Einheit einzusetzen. [**OSSEC**](http://www.ossec.net/) ist ein Open Source Intrusion Detection System, das auch file integrity checks unterstützt. Der Vorteil ist, dass OSSEC oder ein vergleichbares System unter Umständen schon vorhanden ist und zudem durch Netzwerksicherheit noch zusätzlichen Mehrwert liefert. Abbildung 2 zeigt die Grundlegende funktionsweise von OSSEC. Der OSSEC besteht zum einen aus dem OSSEC Server - die zentrale Management Einheit - und den Agenten, einer Client-Software, die auf den zu überwachenden Servern installiert wird. Die Agenten liefern dem Server die notwendigen Syslogs und Events, die dort dann ausgewertet und als Alerts weiter gegeben werden. Bei einer heterogenen und stark verteilten Landschaft bietet sich eine solche Lösung an.

![ossec](fig/ossec-arch2.jpg)

Alternativ sind mit anderen frei verfügbaren Mitteln solche Integritätschecks einfach umsetzbar. Werkzeuge wie **md5sum** oder **shasum** sind für solche Aufgaben perfekt geeignet. Mit einfachen Scripten können Verzeichnisse und Dateien auf Integrität und Änderungen geprüft werden. Bei Änderung in der Zerzeichnisstruktur oder an Dateien wird ein Alert generiert. Der folgende Pseudocode verdeutlicht das Verfahren.

	lastfiles = os.listdir(watched_dir)
	lastnfiles = len(files)
	lastchecks = []
	for file in files: lastchecks.add(checksum(file))
	iswatching = true	
	while iswatching:
		files = os.listdir(watched_dir)
		nfiles = len(files)
		if nfiles != lastnfiles: alert "number files changed"
		checks = []
		for file in files: checks.add(checksum(file))
		if lastchecks != checks: alert "%s files changed" % (diff(checks, lastchecks))
		lastfiles = files
		lastnfiles = nfiles
		lastchecks = checks

Es können also Änderungen in der Verzeichnisstruktur sowie an einzelnen Dateien erkannt werden. Informationen über die Änderungen können dann entweder direkt an den Crawler oder einen dedizierten Watch-Server geschickt werden. Diese Lösung lässt sich mit Hilfe plattformunabhängiger Scriptsprachen wie Python sehr einfach umsetzen. Natürlich muss die Lösung an die jeweilige Verzeichnisstruktur individuell angepasst werden, was aber vernachlässigbarer Aufwand ist.

Während lokale Push Mechanismen also eine effektive und effiziente Möglichkeit zur Erkennung von Änderungen an Webseiten darstellen, haben sie einen entscheidenden und offensichtlichen Nachteil. Sie erfordern selbst Änderungen in der Inrastruktur der hostenden Organisation - falls solche Mechanismen nicht ohnehin schon vorhanden sind - und somit zusätzliches Expertenwissen und weiteren Administrationsaufwand. In vielen Fällen ist dies eventuell unerwünscht oder vielleicht gar nicht möglich. Die Anwendbarkeit von Push Mechanismen ist daher von der jeweiligen Organisation abhängig und darf nicht als Vorraussetzung, sondern lediglich als ergänzende Maßnahme betrachtet werden. Sie muss individuell an die Organisation angepasst werden und daher bleibt ein allgemeiner Nutzen eher fragwürdig.

## Pull Mechanismen

Während bei Push Mechanismen die Alerts bezüglich Änderungen von den Ziel-Domains erzeugt werden, muss bei Pull Mechanismen der Crawler selbst aktiv werden und nach Änderungen suchen. Dies wirft natürlich das Problem auf, dass man Änderungen nur durch Vergleiche feststellen kann, und man eigentlich die komplette Ziel Domain scannen müsste. Um dies zu verhindern muss man zwangläufig samplen, also Stichproben für die Vergleiche heranziehen. Man prüft also lediglich ein Subset der in der Domain auftretenden Dokumente auf Änderung.

Eine relativ einfache und doch aussagekräftige Möglichkeit ist es, News / RSS / Atom feeds auszuwerten. Unter der Annahme, Domains von Organisationen haben bieten mehrere RSS Feeds an, so ist es erneut mit realtiv geringem Aufwand möglich, diese automatisch auszuwerten. Das folgende Code Beispiel zeigt, wie mit wenigen Zeilen Shell-Code RSS Feeds neue RSS Feeds ausgewertet werden können.

	rsstail -i 3 -u example.com/rss.xml -n 0 | while read line
  	do
    	echo "new feed: $line"
  	done

Außer dem Auswerten von RSS Feeds gibt es noch die Möglichkeit einzelne Seiten in definierten Intervallen zu Samplen und von diesen die Änderungsfrequenz herzuleiten. Haupteingangseiten zeigen hierbei oftmals die schnellsten Änderungen, da hier auch Neuigkeiten gepostet werden. 

## Manuelle Konfiguration

Für viele Domains, deren Strukturen und Verwendung bekannt sind, stellt wahrscheinlich die manuelle Konfiguration die effektivste Möglichkeit dar, crawlingraten festzulegen. Beispielsweise müssen Seiten, die den Vorlesungsbetrieb repräsentieren nur zum Ende des Semesters archiviert werden, da im Laufe des Semesters immer nur Neue Informationen hinzukommen, aber darüber hinaus keine Änderungen mehr stattfinden. Durch solches Wissen kann das Crawling erheblich optimiert werden. Solche Frequenzen ließen sich prinzipiel auch automatisiert feststellen, benötigen aber durch den halbjährigen Rhythmus eine sehr lange Lernphase.

## Evaluieren von Änderungen

Unabhängig davon, ob Änderungen per Pull, Push oder Manuell festgestellt werden, müssen irgendwie die Änderungen evaluiert und geeignete crawlingraten bestimmt werden. Es bietet sich an, dies mit Hilfe eines dedizierten WatchServers zu bewerkstelligen. Dieser sammelt zentral Informationen über Änderungen durch verschiedene Verfahren, wie eMail-Notifikationen, Alerts durch File Integrity Checks, Übertragung von diffs oder anderen Events, und wertet diese aus. Hierbei können die verschiedenen Push und Pull Mechanismen beliebig kombiniert werden. Des weiteren sollten die diffs des Crawlers selbst mit evaluiert werden. Das System ist Skizziert in der folgenden Abbildung.

![watch](fig/watch.pdf)

Ein Vorteil einer solchen Lösung ist auch, dass Reporte erstellt und ggf graphisch aufgearbeitet werden können. Hierfür gibt es bereits open source Lösungen, die als Orientierung dienen können. Ein passendes Beispiel liefern Code-Versionierungssysteme wie Git. [Github](github.com) oder auch das quelloffene [Gitlab](http://gitlab.org) stellen zum Beispiel die Änderungen in den Code-Repositorien in Unterschiedlicher Weise dar. Das folgende Bild zeigt einen Graphen der Code Änderungen eines Github Projektes präsentiert. Da Webseiten ebenfalls aus Quellcode bestehen bietet sich der Einsatz eines Code-Versionierungssystem zur Aufbereitung und Auswertung von Änderungen durchaus an. So können individuell für Domains Reporte relativ einfach erzeugt werden.

![gitreport](fig/gitreport.png)

Um geeignete Crawlingraten festzustellen ist es zunächst notwendig eine Datenbasis als Grundlage aufzubauen. Hierfür eignen sich vor allem regelmässige Crawls und die daraus resultierenden Diffs. Diese zeigen bereits Änderungen der Webseiten in Abhängigkeit zur Frequenz der Crawls. Hohe Crawlingraten führen dabei zu schnellerer Bereitstellung von Daten, zeigen aber eventuell nur wenige Änderungen. Niedrige Crawlingraten dagegen führen zu einem langsameren Aufbau des Datenbestands, zeigen aber dafür wahrscheinlich mehr Änderungen. Auch hier ist oftmals Expertenwissen über Domains sinnvoll. Indikator ist in jedem Fall das Verhältnis zwischen Anzahl der Änderungen bzw. Größe der Diffs zu Frequenz der crawls bzw der Änderungsevents. Liegt dieses Verhältnis über einem vorher definiertem Schwellenwert wird ein neuer Crawl automatisch ausgelöst, bzw die bisherige Crawlingrate angepasst.



