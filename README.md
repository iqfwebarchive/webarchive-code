Copyright (c) 2011-2017 thomas.zink _at_ uni-konstanz _dot_ de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

# IQF WebArchive - Kooperative und nachhaltige Archivierung von Webinhalten an Hochschulen

**Note: This project concluded in 2014 and is no longer maintained**

This is the documentation and source code of some of the packages for the [IQF Project "Kooperative und nachhaltige Archivierung von Webinhalten an Hochschulen"](webarchive.uni-konstanz.de).
It is a german project, that ran from 2011 to 2014 and was funded by the "Innovations- und Qualitätsfonds (IQF)" of the state Baden-Württemberg.
This means, the project webpage and all documentation is in german (except for source comments).

Below is a short description of the work packages.

## Webharvesting of Publication Data

Harvest and structure publication data from webpages.

## Crawlrate

Determine an efficient and effective crawlrate, at which the pages are crawl and data is harvested.

## Detect Webservers

Identify webservers associated with a specific organisation.




