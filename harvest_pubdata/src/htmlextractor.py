#! /usr/bin/env python
# -*- coding: utf-8 -*-
###
# Copyright (c) 2011-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
'''
This module implements functions to extract HTML pages from variable sources.
'''

__all__ = ["URLExtractor"]

# fetch urls
import urllib2


class HeritrixCrawlLogExtractor:
    def __init__(self,logfilename):
        handle = open(logfilename,'r')
        self.urls = parse_heritrix_crawl_log(handle)
        handle.close()
        self.generator = url_html_generator(self.urls)


# this one is inefficient if html is not needed multiple times
def heritrix_crawl_log_extractor(logfilename):
    handle = open(logfilename,'r')
    urls = parse_heritrix_crawl_log(handle)
    handle.close()
    htmlmap = {}
    for url in urls:
        htmlmap[url] = get_url(url)
    return htmlmap
    
def parse_heritrix_crawl_log(logfilehandle):
    urls = set()
    for line in logfilehandle:
        split = line.split()
        if split[6] == 'text/html':
            urls.add(split[3])
    return list(urls)

def url_html_generator(urls):
    for url in urls:
        yield url, get_url(url)

def get_url (url):
    try:
        return urllib2.urlopen(url).read()
    except urllib2.HTTPError:
        return None
    except urllib2.URLError:
        return None

### MAIN
def usage():
    print "usage:", sys.argv[0], "<path/to/Heritrix/crawl/logfile>"
    
if __name__=='__main__':
    import sys

    if len(sys.argv) != 2:
        sys.exit(usage())
    
    fname = sys.argv[1]
    try: ex = HeritrixCrawlLogExtractor(fname)
    except: sys.exit(usage())

    print
    print "%s URLS" % len(ex.urls)
