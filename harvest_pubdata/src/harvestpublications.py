#! /usr/bin/env python
# -*- coding: utf-8 -*-
###
# Copyright (c) 2011-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
"""
This script harvests publication data from URLs.
To do that it performs the following:

1. Parse a heritrix crawl log file (input) and extract URLs
2. Extract HTML from the URLs.
3. Check the HTML for possible publications based on regex patterns
4. Structure the publication data using a citation parser
"""
import sys
import htmlextractor as htmlex
import publicationextractor as pubex
import citationparser as citex
import time # for less aggressive requests

def usage():
    print "usage:", sys.argv[0], "<path/to/Heritrix/crawl/logfile>"

def harvest(generator):
    pubdict = {}
    for url, html in generator:
        #print "Check", url
        if html:
            pubs = pubex.extract_publications(url,html)
            print "Found", len(pubs), "Publication Candidates on", url
            if len(pubs) > 0:
                pubdict[url] = pubs
        #else:
        #    print "ERROR"
    return pubdict

def extractcitation(engine,publists):
    for publist in publists:
        for pub in publist:
            pub.bib = engine(pub.string)
            print pub.as_xml()
        
    

if __name__=='__main__':
    if len(sys.argv) != 2:
        sys.exit(usage())

    fname = sys.argv[1]
    print "Parse URLs from logfile", fname
    try:
        extractor = htmlex.HeritrixCrawlLogExtractor(fname)
    except Exception as e:
        print e
        sys.exit(usage())

    # crawl urls
    print "Harvest publications from", len(extractor.urls), "URLs"
    pubdict = harvest(extractor.generator)

    print "Found", sum(map(len,pubdict.values())), "Publication Candidates",
    print "on", len(pubdict.keys()), "Publication Sites."

    print "Trying to extract citation data"
    engine = citex.FreeCite()
    extractcitation(engine,pubdict.values())

