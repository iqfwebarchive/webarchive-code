#! /usr/bin/env python
# -*- coding: utf-8 -*-
###
# Copyright (c) 2011-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
'''
This script provides functions to extract
unstructured publication data from a webpage.

It does this by:

1. Checking if the webpage contains any publication at all
2. Find publication candidates
3. Extract Strings and Anchors for publication candidates

The script takes an URL as input.
It outputs the identified publication data in XML or JSON format (default JSON).

Requirements:

- BeautifulSoup (http://www.crummy.com/software/BeautifulSoup/)
'''

import sys

# requires BeautifulSoup
try:
	from bs4 import BeautifulSoup
except ImportError:
	try:
		from BeautifulSoup import BeautifulSoup
	except ImportError:
		print('FATAL: could not import BeautifulSoup')
		print('Download from http://www.crummy.com/software/BeautifulSoup/')
		sys.exit(1)

# for fetching urls
import urllib2

# for regular expressions
import re

# for patterns
import pattern

# for json output
import json

import sys

def usage():
        print "usage:", sys.argv[0], "<url>"


class Publication(object):
        # url where the publication has been found
        url = ''
        
        # list of possible search strings found on the webpage
        string = ''

        # list of possible links to the publication data
        links = []

        # string of possible bib entries retrieved from citation parser
        bib = ''

        def __init__ (self,pub):
                self.string, self.links = self.parse_publication(pub)

        def __str__(self):
                return "[%s,%s,%s]" % (self.string,self.links,self.bib)

        def __repr__(self):
                return str(self)

        def as_xml(self):
                s = u"<publication>\n"
                s += u"\t<url>" + u''.join(self.url).encode("utf-8") + u"</url>\n"
                s += u"\t<string>" + self.string.decode("utf-8") + u"</string>\n"
                s += u"\t<links>\n"
                for i in self.links:
                        s += u'\t\t<link>' + u''.join(i).encode("utf-8") + u'</link>\n'
                s += u"\t</links>\n"
                s += u'\t\t<bib>' + self.bib + u'</bib>\n'
                s += u"</publication>\n"
                return s

        def as_json(self):
                s = u"{\n"
                s += u"\turl: " + u''.join(self.url).encode("utf-8") + u"\n"
                s += u"\tstring: " + self.string.decode("utf-8") + u"\n"
                for i in self.links:
                        s += u'\tlink: ' + u''.join(i).encode("utf-8") + u'\n'
                if self.bib != '': s += u'\tbib: ' + self.bib + u'\n'
                s += u"}\n"
                return s

        def parse_publication(self,pub):
                '''
                parse_publication -- try to find strings and anchors
                pubs - list of publication candidates
                returns lists of found strings and anchors
                '''
                string = ''
                links = []

                # get iterator of all strings: deprecated, now directly in .text
                #if pub.strings != None:
                #        it = pub.strings
                #        for i in it:
                #                strings.append(i)
                if pub.text != None:
                        string = u''.join(pub.text.strip()).encode("utf-8")
                        #strings.append(pub.text)

                # find anchors
                anchors = pub.findAll('a')
                for anchor in anchors:
                        attrs = anchor.attrs
                        if type(attrs) == type({}):
                                try: links.append(attrs['href'])
                                except KeyError: continue
                        elif type(attrs) == type([]):
                                for attr in attrs:
                                        if attr[0] == 'href':
                                                links.append(attr[1])
                return string, links
                                             

# Check Web Page for Publications
def has_publications(html):
        '''
        checks a web page if it might contain publications.
        html - html of the webpage
        returns True if likely to have publications, False otherwise
        '''
        m = pattern.pubpage.findall(html)
        return len(m)>0

# Parse a Publication Page
def parse_html(html):
        '''
        parse_html -- read and parse html
        html - html string
        returns the parsed html file
        '''
        #html = open(htmlfile,'r').read()
        parsed = BeautifulSoup(html,"lxml")
        return parsed

# Find Publication Candidates
def find_publications(parsed_html):
        '''
        find_publications -- find sets of publication candidates
        Tries to do that by retrieving all lists, tables, and paragraphs on the page
        parsed_html - the parsed htlm object
        returns a list of publication candidates
        '''
        # init list of empty publication candidates
        candidates = []
        # find table rows, list entries, paragraphs
        li = parsed_html.findAll('li') # findAll instead of find_all ... ?
        p = parsed_html.findAll('p')
        tr = parsed_html.findAll('tr')
        # extend the candidate list
        candidates.extend(tr)
        candidates.extend(li)
        candidates.extend(p)
        # check the candidates
        pubs = []
        for c in candidates:
                if pattern.isCitation(c.text):
                        pubs.append(Publication(c))
        # return
        return pubs

# Do everything
def extract_publications(url, html):
        '''
        Extracts Publications from HTML pages.
        url - the url of the webpage
        html - the html of the webpage
        returns a list of identified publications
        '''
        publist = []
        if has_publications(html):
                parsed = parse_html(html)
                publist = find_publications(parsed)
                for p in publist:
                        p.url = url
        return publist
                
 
if __name__=='__main__':
        if len(sys.argv) != 2: sys.exit(usage())
        url = sys.argv[1]

        try:
                html = urllib2.urlopen(url).read()
        except urllib2.HTTPError as e:
                print e
                sys.exit(usage())

        publist = extract_publications(url,html)
        for p in publist:
                print p.as_json()
                #print p.as_xml()
