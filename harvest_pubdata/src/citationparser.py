# -*- coding: utf-8 -*-
###
# Copyright (c) 2011-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
'''
This module implements functions to structure unstructured publication
data using external databases.
'''

__all__ = ["GoogleScholar", "CiteSeer", "FreeCite"]

import sys
import urllib2
import urllib

# requires BeautifulSoup
try:
	from bs4 import BeautifulSoup
except ImportError:
	try:
		from BeautifulSoup import BeautifulSoup
	except ImportError:
		print('FATAL: could not import BeautifulSoup')
		print('Download from http://www.crummy.com/software/BeautifulSoup/')
		sys.exit(1)

try:
        import scholar
except ImportError:
        print('FATAL: could not import scholar.py')
        print('Download from https://github.com/ckreibich/scholar.py') 
        sys.exit(1)

# TODO integrate crossRefQuery.
# Problems with dependencies: httplib2
#try:
#        import crossrefquery
#except ImportError:
#        print('FATAL: could not import query.py')
#        print('Download from https://github.com/MartinPaulEve/crossRefQuery/query.py') 
#        sys.exit(1)


# format the strings correctly to enable web queries
def format_strings(string):
        return u''.join(string).encode('utf-8').strip()

# Abstract base class for search engines
class CitationParser (object):
        def __init__ (self):
                raise NotImplementedError("Abstract Base Class")
        
        def __call__ (self, string):
                #string = format_strings(string)
                # check if string satisfies conditions
                if len(string.strip().split()) <= 3: return
                return self._query(string)

        def _query (self, string):
                raise NotImplementedError("Method must be implemented")

# Search Engine Implementations
class GoogleScholar (CitationParser):
        def __init__ (self): pass

        def _query(self,string):
                bib = []
                querier = scholar.ScholarQuerier()
                settings = scholar.ScholarSettings()
                settings.set_citation_format(scholar.ScholarSettings.CITFORM_BIBTEX)
                querier.apply_settings(settings)
                query = scholar.SearchScholarQuery()
                #print "send query for", string
                query.set_words(string)
                querier.send_query(query)
                articles = querier.articles
                for art in articles:
                        #print art.as_citation()
                        bib.append(art.as_citation())
                return bib
                        

class CiteSeer (CitationParser):
        def __init__ (self): pass

        def _query (self,string):
                bib = []
                url = "http://citeseerx.ist.psu.edu/search?q="
                q = "+" + string.replace(" ", "+") + "&submit=Search&sort=rlv&t=doc" # sort=cite
                url += q
                #print "fetch url", url
                soup = BeautifulSoup(urllib2.urlopen(url).read())
                results = soup.html.body("div", id = "result_list")
                for result in results:
                        #print result
                        try:
                                title = result.h3.a.text.strip()
                                authors = result("span","authors")[0].string
                                authors = authors[len("by "):].strip()
                                bib.append('{ author = "' + authors + '", title = "' + title + '" }')
                        except:
                                pass
                return bib

class FreeCite (CitationParser):
        url = 'http://freecite.library.brown.edu/citations/create'
        header = ["Accept", "text/xml"]
        
        def __init__ (self): pass

        def _query(self,string):
                req = urllib2.Request(self.url)
                req.add_header(*self.header)
                req.add_data(urllib.urlencode({"citation" : string}))
                resp = urllib2.urlopen(req)
                soup = BeautifulSoup(resp.read())
                bib = soup.prettify()
                return bib
