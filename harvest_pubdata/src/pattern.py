# -*- coding: utf-8 -*-
###
# Copyright (c) 2011-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
import re

pubpage = re.compile("<(title|h).*>.*(Ver(ö|&ouml)ffentlichung|Publikation|Publication|Schriften).*</(title|h).*>",re.IGNORECASE)

# author, name*: Title; *(year), *
htwg0 = re.compile(r'\w+,\s*\w+.*:\s*[a-zA-Z0-9_\s-]*;.*\(\d{4}\).*')
# author, name*: Title; *year,*
htwg1 = re.compile(r'\w+,\s*\w+.*:\s*[a-zA-Z0-9_\s-]*;.*\d{4}.*')
# author, name*. "Title.".*(year): *
mla = re.compile(r'\w+,\s*\w+.*\."[a-zA-Z0-9_\s-]*\."\s*\w+.*\(\d{4}\):.*')
# author, N.* (year). Title. *,*
apa = re.compile(r'\w+,\s*\w\..*\(\d{4}\)\.\s*[a-zA-Z0-9_\s-]*\.\s*.*,.*')
# autor, name*. Title. *, year, *
iso = re.compile(r'\w+,\s*\w+.*\.\s*[a-zA-Z0-9_\s-]*\..*,\s*\d{4},.*')


citationstyles = [htwg0,htwg1,mla,apa,iso]

def isCitation(s):
        s = s.strip()
        if s=='' or s=='\n' or s==None: return False
        #print '<Candidate>'
	#print "\t",s
	ret = False
	for style in citationstyles:
		ret |= (style.findall(s) != [])
		if ret == True:
                    #print 'True:', style.pattern
                    break
        #print '</Candidate>'
	return ret


def test():
	# teststrings
	shtwg = 'Zimmermann, Stephan; Rentrop, Christopher: Schatten-IT; In: HMD - Praxis der Wirtschaftsinformatik 49 (2012), 288, S. 60-68.'
	smla = 'Zimmermann, Stephan, and Christopher Rentrop. "Schatten-IT." HMD Praxis der Wirtschaftsinformatik 49.6 (2012): 60-68.'
	sapa = 'Zimmermann, S., & Rentrop, C. (2012). Schatten-IT. HMD Praxis der Wirtschaftsinformatik, 49(6), 60-68.'
	siso = 'ZIMMERMANN, Stephan; RENTROP, Christopher. Schatten-IT. HMD Praxis der Wirtschaftsinformatik, 2012, 49. Jg., Nr. 6, S. 60-68.'
	print isCitation(shtwg)
	print htwg.findall(shtwg)
	print mla.findall(shtwg)
	print apa.findall(shtwg)
	print iso.findall(shtwg)
	print isCitation(smla)
	print htwg.findall(smla)
	print mla.findall(smla)
	print apa.findall(smla)
	print iso.findall(smla)
	print isCitation(sapa)
	print htwg.findall(sapa)
	print mla.findall(sapa)
	print apa.findall(sapa)
	print iso.findall(sapa)
	print isCitation(siso)
	print htwg.findall(siso)
	print mla.findall(siso)
	print apa.findall(siso)
	print iso.findall(siso)

if __name__ == '__main__':
	test()
