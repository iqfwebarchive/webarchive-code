# Webharvesting of Publication Data

Harvests publication data from URLs, strucure identified publication data,
and print out as JSON or XML.

## Usage

### Crawler and Publicationextractor

The crawler needs a Heritrix crawl log as input.

	harvestpublications.py <path/to/Heritrix/crawl/logfile>

### Publicationextractor only

The publicationextractor uses an URL as input, checks it for publications and outputs structured publication data.

    publicationextractor.py <URL>

## Requirements

* [BeautifulSoup](http://www.crummy.com/software/BeautifulSoup/)
* [scholar.py](https://github.com/ckreibich/scholar.py)
